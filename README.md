# Комплект документов для вакансии веб-разработчика в компанию ООО "АЛВИК"

Документы:

* [Описание вакансии](position-description.md).
* [Тестовое задание](test-assignment.md).
* [План собеседования](interview-questions.md).

Ориентировался на вакансии:

* [Ruby on Rails разработчик](https://chelyabinsk.hh.ru/vacancy/15704509).
* [Программист Web/PHP](https://chelyabinsk.hh.ru/vacancy/17138385).

Сервисы по скриннингу программистов:

* [TestDome](https://www.testdome.com/Plans).
* [Codility](https://codility.com/pricing/).
* [Interview Zen](https://www.interviewzen.com).

Статьи по подбору программистов:

* [How to Hire a Programmer](https://blog.codinghorror.com/how-to-hire-a-programmer/).
