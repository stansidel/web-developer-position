# Test Questions for a Web Developer

1. Consider the following code:

    ```php
    $arNames = ["Steve", "John", "Ivan", "Julie"];
    for ($i = 0; $i < 4; $i++) {
        echo 'Hey ' . $arNames[$i] . '\n';
    }
    ```

    What would this code output? Would you rewrite it in any way?

2. Given the following HTML piece:

    ```html
    <ul>
        <li style="font-weight: bold; color: blue;">First Item</li>
        <li style="color: blue;">Second Item</li>
        <li style="color: blue;">Third Item</li>
        <li style="color: blue;">Fourth Item</li>
        <li style="color: blue;">Fifth Item</li>
    </ul>
    ```

    How would you make it more HTML5 compliant?

3. Given the following HTML:

    ```html
    <div>
        <p id="header">Cool Header-Style Paragraph</p>
    </div>
    <a href="#" id="make_header_red">Make the header red</a>
    ```

    Write code in pure JavaScript that would change text color of the `<p>` element to red when the link is clicked.

4. Do the same thing as in the previous task but using jQuery.
5. Given the following HTML:

    ```html
    <div class="container">
        <div class="central">
            <p>I wanna be always visible.</p>
        </div>
        <div class="sidebar">
            <p>Hide me on mobile</p>
        </div>
    </div>
    ```

    And corresponding CSS:

    ```css
    .container .central {
        width: 65%;
        display: inline-block;
    }
    .container .sidebar {
        width: 30%;
        display: inline-block;
    }
    ```

    Change the CSS so that the `sidebar` stays visible, on the right from the `central`, on wide screens only. It should be hidden on screens with width less than 320px.

6. You're working with a MySQL database. There's a table named `Items` with the following data:

    |   ID    |  Name   |   Price   |
    |:-------:|---------|----------:|
    |    1    | Item 1  |  1278.10  |
    |    2    | Item 2  |   500.30  |
    |    3    | Item 3  |  3272.10  |

    How do you select all items (ID, Name and Price) with price over 1000?

7. You're in the root of your git repository. `git status` gives the following output:

    ```
    On branch master
    Changes not staged for commit:
      (use "git add/rm <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   index.html
        deleted:    test.php

    Untracked files:
      (use "git add <file>..." to include in what will be committed)

        README.md

    no changes added to commit (use "git add" and/or "git commit -a")
    ```

    Write the commands to commit all the changes and push them to the remote `origin`.

